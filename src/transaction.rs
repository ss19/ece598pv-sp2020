extern crate untrusted;
use serde::{Serialize,Deserialize};
use ring::signature::{Ed25519KeyPair, Signature, KeyPair, VerificationAlgorithm, EdDSAParameters};
use bincode;

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct Transaction {
	Input: String,
	Output: String,
}

/// Create digital signature of a transaction
pub fn sign(t: &Transaction, key: &Ed25519KeyPair) -> Signature {
	let MESSAGE: &[u8] = &bincode::serialize(t).unwrap();
	let signature: Signature = key.sign(MESSAGE);
	return signature;
}

/// Verify digital signature of a transaction, using public key instead of secret key
pub fn verify(t: &Transaction, public_key: &<Ed25519KeyPair as KeyPair>::PublicKey, signature: &Signature) -> bool {
	let message: &[u8] = &bincode::serialize(t).unwrap();
	let pk = untrusted::Input::from(public_key.as_ref());
	let msg = untrusted::Input::from(message);
	let sig = untrusted::Input::from(signature.as_ref());
	let correct_flag: bool = EdDSAParameters.verify(pk, msg, sig).is_ok();
	return correct_flag;
}

#[cfg(any(test, test_utilities))]
mod tests {
    use super::*;
    use crate::crypto::key_pair;

    pub fn generate_random_transaction() -> Transaction {
        let t = Transaction {
			Input: String::from("hello"),
			Output: String::from("world"),
	};
	return t
    }

    #[test]
    fn sign_verify() {
        let t = generate_random_transaction();
        let key = key_pair::random();
        let signature = sign(&t, &key);
        assert!(verify(&t, &(key.public_key()), &signature));
    }
}
